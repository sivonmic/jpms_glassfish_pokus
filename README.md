Nemám čas takže iba stručne (zatiaľ)

**Požiadavky**
- JDK 11
- payara 5.2020


**Ako zapnúť túto obludnosť**

Budete musieť zmeniť konfiguráciu JVM vo vašej payare aby to fungovalo.

1. Zapnite svoju payra doménu.
1. Niekde na počítači je jedno kde si vytvorte priečinok `apiModules`.
1. Choďte do admin konzoly vašej domény na pyare.
1. Pridajte nastavenia JVM  (Configurations-> najdite práve bežiacu konfiguráciu -> JVM settings -> JVM Options):
    - --module-path=/Cesta/ku/priečinku/ktorý/ste/vytvorili/vyššie
    - --add-modules=ModuleAPI
    - --add-modules=ALL-SYSTEM
1. Import do intellij alá maven
1. Pridajte si kunfiguráciu alá *remote* glassfish. (V rámci konfigurácie voláte vždy pred nasadením maven goal package a nasadzujete *war* artifakt)
1. Zavolajte maven goal package ručne z tej bočnej lišty v intellij (aby sa vám skompiloval jar s API)
1. Vezmite skompilovaný jar s api (`ModuleAPI-1.0-SNAPSHOT.jar`) a presuňte ho do priečinku vytvoreného vyššie.
1. Reštartujte payaru.
1. Nasaďte *war* aplikácie na payaru.
1. Choďte na http://localhost:8080/WebApp-1.0-SNAPSHOT/uploadModule
1. Uploadujte súbor `HelloImplemented-1.0-SNAPSHOT.jar` (ak to vybuchuje zapnite si debugger a skontrolujte v prvom rade či sedia cesty k jar uploadovanému na pyaru, neviem ako sa to bude správať vo windowse tak si prípadne opravte tie paths tak aby to našlo ten uploadovaný jar).
1. Choďte na http://localhost:8080/WebApp-1.0-SNAPSHOT/api/hello zavolá sa interface a vnútorné trieda jaru, ktorý ste práve uploadovali.

**Ako sa s tým pohrať**

Následne môžte zmeniť implemntáciu HelloImplemented a znovu ju uploadovať ako vyššie - voilá prebehne runtime-swapping modulu.

**Dôležitá poznámka**

Mal som OBROVSKÝ problém to umlátiť tak aby to fungovalo z dôvodou o ktorých nemám čas sa tu rozpisovať. Môžte si prečítať ten architectura.docx na sharepointe tam som napísal trochu viac (sorry za preklepy nemám čas to kontrolovať).

V prípade, že chcete zmeniť to interface (ModuleAPI-1.0-SNAPSHOT.jar) tak ho musíte znovu zkompilovať, presunúť ten skompilovaný jar do toho priečinku a **REŠTARTOVAŤ** payaru (za načítanie toho modulu musí byť zodpovedné JVM ešte pred tým než sa zapne payara z komplikovaných dôvodov o ktorých som sa trošku rozpísal na sharepointe ak vás to zaujíma).

Ak niečo nefunguje napíšte mi na slack ale u mňa to šlape no problemo.
