package publicClasses;


import api.HelloInterface;
import dataTransfer.HelloTransfer;
import privateClasses.SomePrivateClass;

public class HelloImplemented implements HelloInterface {
    private SomePrivateClass somePrivateClass;

    public static HelloInterface provider() {
        return new HelloImplemented();
    }

    private HelloImplemented() {
        somePrivateClass = new SomePrivateClass();
    }

    @Override
    public HelloTransfer transferHello() {
        return new HelloTransfer(somePrivateClass.getHello());
    }

    @Override
    public String doHello() {
        return somePrivateClass.getHello();
    }
}
