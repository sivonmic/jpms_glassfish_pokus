import api.HelloInterface;
import publicClasses.HelloImplemented;

module HelloImplemented {
    requires ModuleAPI;
    exports publicClasses;
    provides HelloInterface with HelloImplemented;
}