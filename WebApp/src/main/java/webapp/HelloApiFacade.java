package webapp;

import api.HelloInterface;
import dataTransfer.HelloTransfer;

import javax.ejb.Singleton;
import java.lang.module.Configuration;
import java.lang.module.ModuleFinder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


@Singleton
public class HelloApiFacade implements HelloInterface {
    private static final Logger logger = Logger.getLogger(HelloApiFacade.class.getName());

    private final String UPLOADED_MODULES_DIR = "/modules"; //sem uploadujeme modul s implementaciou

    private ModuleLayer currentLayer;

    HelloInterface helloInterface;

    @Override
    public String doHello() {
        if (helloInterface == null) {
            return null;
        } else {
            return helloInterface.doHello();
        }
    }

    @Override
    public HelloTransfer transferHello() {
        if (helloInterface == null) {
            return null;
        } else {
            return helloInterface.transferHello();
        }
    }

    @Override
    public void reloadModule(String applicatioPath) {
        Path pathToModules = Paths.get(applicatioPath + UPLOADED_MODULES_DIR); //ak to tu vybuchuje overte ze sedia cesty k jarom, neviem ako sa to bude spravat na windowse

        ModuleFinder moduleFinder = ModuleFinder.of(pathToModules);
        ModuleLayer parent = ModuleLayer.boot();
        Configuration cf = parent.configuration().resolve(moduleFinder, ModuleFinder.of(), Set.of("HelloImplemented"));
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        currentLayer = parent.defineModulesWithOneLoader(cf, classLoader); //garbage collector zlikviduje staru vrstvu, overene

        Iterable<HelloInterface> helloServices = ServiceLoader.load(currentLayer, HelloInterface.class);
        int counter = 0;
        for (HelloInterface helloInterface : helloServices) {
            this.helloInterface = helloInterface;
            counter++;
        }

        if (counter != 1) {
            logger.log(Level.SEVERE, "Mala by byt nacitana presne jedna implementacia rozhrania v extrenom module." +
                    " Bola nacitana ziadna alebo viac implementacii. Toto je s velkou pravdepodobnostou zavazna chyba.");
        }
    }
}
