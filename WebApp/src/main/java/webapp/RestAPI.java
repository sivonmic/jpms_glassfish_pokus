package webapp;


import api.HelloInterface;
import dataTransfer.HelloTransfer;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Singleton
@Path("/")
public class RestAPI {

    @Inject
    private HelloInterface helloApiFacade;

    @GET
    @Path("hello")
    public String getHello() {
        String hello = helloApiFacade.doHello();
        if (hello == null) {
            return "Nebezi";
        } else {
            return hello;
        }
    }

    @GET
    @Path("transferHello")
    public String transferHello() {
        HelloTransfer hello = helloApiFacade.transferHello();
        if (hello == null) {
            return "Nebezi";
        } else {
            return hello.getHello();
        }
    }
}
