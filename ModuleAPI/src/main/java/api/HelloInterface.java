package api;

import dataTransfer.HelloTransfer;

public interface HelloInterface {
    String doHello();

    HelloTransfer transferHello();

    default void reloadModule(String pathToJar) {
        throw new UnsupportedOperationException("Tat metoda nie je implementovana. Pravdepodobne sa ju pokusas zavolat na implementacii z vnutra" +
                "svapovatelneho modulu.");
    }
}
