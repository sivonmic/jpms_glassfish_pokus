package dataTransfer;

public class HelloTransfer {
    String hello;
    int foo;

    public HelloTransfer(String hello) {
        this.hello = hello;
        foo = 42;
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

    public int getFoo() {
        return foo;
    }

    public void setFoo(int foo) {
        this.foo = foo;
    }
}
